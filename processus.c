#include "processus.h"
#include <cpu.h>

process p[NOMBRE];
int32_t count = 0,  count2=0; //count pid of the last processus, count2 iterator for ordonnance

process *actif; 

void init_proc()
{
    //main function
    for(int i = 0; i <NOMBRE; i++)
    {
        p[i].pid=-1; //supposed to be a invalid value
        p[i].etat = MORT; 
    }
    p[0].s = "idle";
    p[0].etat = ELU; //a proccess needs to be active
    p[0].pid = 0;
    actif= &p[0]; //actif process
}
int32_t cree_processus(void (*func)(void), char *s)
{
    if(count == NOMBRE-1)
    {
        return -1; 
    }
    
    for (int i = 0; i < NOMBRE; i++)
    {
        if(p[i].pid == -1)
        {
            p[i].pid = i; 
            p[i].s = s; 
            p[i].etat = ACTIVABLE;
            p[i].zones[0] = (uint64_t)func; //ra
            p[i].zones[1] = (uint64_t)(&(p[i].pile[4095])); //sp->pile[4095] top 4095
            count= p[i].pid;
            break; 
        } 
    }
    return count; 
    //count+=1;
}

void ordonnance(void) //pseudoparalelism
{
    process *old; //old process -> context to save

    //reveil 
    for(int32_t i = 0; i<NOMBRE; i++)
    {
        if(((p[i].heureReveil == nbr_secondes()) && (p[i].etat == ENDORMI)))
        {
            p[i].etat = ACTIVABLE;
        }
    }
    old = actif; 
    
    for(int32_t i=(actif->pid+1);i<(NOMBRE+1); i++)//the problem is  hre, bc proc1 is the  first activable so it keeeeeps channging tto the same
    {
        if(p[i%(NOMBRE)].etat == ACTIVABLE)
        {
            
            actif=&p[i%NOMBRE];
            break; 
        }
    }
    //change of states
    if(old->etat == ELU) 
    {
        old->etat = ACTIVABLE;
    }
    actif->etat = ELU;

    ctx_sw(old->zones, actif->zones);
}
int32_t mon_pid(void)
{
    return actif->pid;
}
char *mon_nom(void)
{
    return actif->s; 
}

void dors(uint32_t nbr_secs)
{
    actif->etat = ENDORMI; 
    actif->heureReveil = nbr_secondes() + nbr_secs;
    ordonnance();
}

/*void fin_processus()
{
    ordonnance();
    actif->etat = MORT;
    ordonnance();
}*/

void idle(void)
{
    for (;;) {
        //printf("Idle pid %d ", mon_pid());
        enable_it();
        hlt();
        disable_it();
    }
}

void proc1(void)
{
    for (;;) {
        printf("[temps = %u] processus %s pid = %i\n",
               nbr_secondes(),
               mon_nom(),
               mon_pid());
        dors(2);
    }
    //fin_processus();
}

void proc2(void)
{
    for (;;) {
        printf("[temps = %u] processus %s pid = %i\n",
               nbr_secondes(),
               mon_nom(),
               mon_pid());
        dors(3);
    }
    //fin_processus();
}
void proc3(void)
{
    for (;;) {
        printf("[temps = %u] processus %s pid = %i\n",
               nbr_secondes(),
               mon_nom(),
               mon_pid());
        dors(5);
    }
    
   //fin_processus();
}
