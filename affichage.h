#include <inttypes.h>
#include <stdio.h>
#include <string.h>

#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#define NULL ((void *)0)


void pixel(uint32_t x, uint32_t y, uint32_t couleur); 
void ecrit_car(uint32_t lig, uint32_t col, char c); 
void place_curseur(uint32_t col, uint32_t lig);
void efface_ecran(void);
void traite_car(char c);
void defilement(void);
void console_putbytes(const char *s, int len);

#endif