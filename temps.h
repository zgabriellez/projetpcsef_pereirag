#include <inttypes.h>
#include <stdio.h>
#include <string.h>

#ifndef TEMPS_H
#define TEMPS_H

extern void mon_traitant(void); 

void affichage_horloge(char *s);
void enable_timer(); 
uint32_t nbr_secondes();
void trap_handler();
void init_traitant_timer(void (*traitant)(void));

#endif