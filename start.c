#include <inttypes.h>
#include <stdio.h>
#include <cpu.h>
#include "affichage.h"
#include "temps.h"
#include "processus.h"

// on peut s'entrainer a utiliser GDB avec ce code de base
// par exemple afficher les valeurs de n et res avec la commande display
// une fonction bien connue

extern process p[NOMBRE];

extern process *actif; 

uint32_t fact(uint32_t n)
{
    uint32_t res;
    if (n <= 1) {
        res = 1;
    } else {
        res = fact(n - 1) * n;
    }
    return res;
}
void kernel_start(void)
{

    init_traitant_timer(mon_traitant);
    enable_timer();

    init_proc();

    cree_processus(&proc1, "proc1");
    cree_processus(&proc2, "proc2");
    cree_processus(&proc3, "proc3");
   
    idle();

    // on ne doit jamais sortir de kernel_start
}