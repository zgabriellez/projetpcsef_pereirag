#include "temps.h"
#include "cep_platform.h"
#include "affichage.h"
#include "processus.h"

#define IT_FREQ 20

uint64_t *p_cmp = 0x02004000, *p_timer = 0x0200bff8;
int32_t interrupt =0, nbr =0, milisec=0, sec=0, minutes=0, hours =0;//number of seconds
char buffer[10];

extern line, column; 

void affichage_horloge(char *s)
{
    int i = 8;
    int x = line, y = column;
    place_curseur(0, 159); 
    while(i>0)
    {
        traite_car('\b');
        i--; 
    }
    place_curseur(0, 150); 
    printf("%s", s);
    place_curseur(y,x);
}
void enable_timer()
{
    //activer interruption
    uint16_t x=0;
    __asm__("csrr %0, mie":: "r"(x));
    x |= 0b10000000; //change bit 7 -> quthorized interruptions
    __asm__("csrw mie, %0":: "r"(x));

    //"utilisation d l'horloge"

    *p_cmp = *p_timer  + TIMER_FREQ/IT_FREQ; 
}

uint32_t nbr_secondes()
{
    return nbr; 
}
void trap_handler(uint32_t mie, uint32_t status, uint32_t mip)
{
    uint8_t j; 
    uint32_t aux = 1; 
    *p_cmp = *p_timer  + TIMER_FREQ/IT_FREQ;
    milisec+=1000/IT_FREQ;
    
    if((status & ~(aux<<31)) == 128)// bit 7 positif after mask
    {           
        interrupt += 1;
    }
    
    if(milisec>=1000)
    {
        milisec-=1000;
        nbr+=1; //seconds of execution
        sec+=1; 
    } 
    if(sec >=60)
    {
        minutes +=1; 
        sec-=60;
    }
    if(minutes >=60)
    {
        minutes-=60; 
        hours +=1; 
    }
    if(interrupt % 19 == 0)
    {
        j = sprintf(buffer, "[%02d:", hours);
        j += sprintf(buffer + j, "%02d:", minutes);
        j += sprintf(buffer + j, "%02d]", sec);
        affichage_horloge(buffer);
    }
    ordonnance();

}
void init_traitant_timer(void (*traitant)(void)) //initialise mtvec
{
    __asm__("csrw mtvec, %0":: "r"(traitant));
}