![lastupdate](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/lastupdate.svg)
[Log du dernier test](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/pcsef.log)

# Affichage

![Affichage_simple](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/Affichage_simple.svg)
![LF_new_line](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/LF_new_line.svg)
![HT_horizontal_tab](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/HT_horizontal_tab.svg)
![BS_backspace](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/BS_backspace.svg)
![CR_carriage_ret](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/CR_carriage_ret.svg)
![Curseur_simple](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/Curseur_simple.svg)
![scroll](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/scroll.svg)
![Curseur_après_scroll](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/Curseur_après_scroll.svg)
![FF_formfeed](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/FF_formfeed.svg)
![Curseur_après_FF](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/Curseur_après_FF.svg)

# Timer

![init_traitant_timer](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/init_traitant_timer.svg)
![enable_timer_mie](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/enable_timer_mie.svg)
![enable_timer_re-active](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/enable_timer_re-active.svg)
![trap_handler_re-active](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/trap_handler_re-active.svg)
![trap_handler_affichage_du_temps](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/trap_handler_affichage_du_temps.svg)
![nbr_secondes](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/nbr_secondes.svg)

# Processus gestion basique

![init_processus_mon_nom_mon_pid](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/init_processus_mon_nom_mon_pid.svg)
![cree_processus_ordonnance_simple](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/cree_processus_ordonnance_simple.svg)
![cree_processus_ordonnance_double](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/cree_processus_ordonnance_double.svg)
![cree_processus_ordonnance_apres_reset](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/cree_processus_ordonnance_apres_reset.svg)
![Processus_par_interruption](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/Processus_par_interruption.svg)

# Processus endormissement

![dors_2_processus_endormis](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/dors_2_processus_endormis.svg)
![dors_reveil_d'un_processus](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/dors_reveil_d'un_processus.svg)
![dors_reveil_de_tous_les_processus](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/dors_reveil_de_tous_les_processus.svg)

# Processus terminaison

![terminaison_explicite](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/terminaison_explicite.svg)
![terminaison_implicite](https://pcsef.pages.ensimag.fr/riscv/seoc2a/evals/ProjetPCSEF_pereirag/terminaison_implicite.svg)
