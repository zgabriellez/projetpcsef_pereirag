#include "affichage.h"


extern char font8x8_basic[256][8];
uint8_t flag_char = 0, flag_cr=0;
uint32_t *curseur, line=0, column=1, aux1, aux2; 

void pixel(uint32_t x, uint32_t y, uint32_t couleur)
{
    uint32_t *pixel;
    pixel = (uint32_t *)(0x80000000 + (x+y*1280)*4);//not-inverted, should be y + x*1280    
    *pixel=couleur;
}

void ecrit_car(uint32_t lig, uint32_t col, char c)
{
    uint8_t mask = 0b00000001; //mask to be used in the program 
    uint8_t i = 0, j = 0; //iterator -- for

    while(i<8)
    {
        for(j = 0; j < 8 ; j++)
        {
            if((font8x8_basic[(uint8_t )c][i] & (uint8_t )(mask << j))>0)
            {
                pixel(lig*8+ j,col*8 + i, 0x00FFFF00);
            }
        }
        i+=1;
    }
}
void efface_ecran(void)
{
    uint32_t i = 0;
    for(i=0; i < 90; i++)
    {
        place_curseur(i,1280/8-1);
        while(line>0) //erase it all until the first position
        {
            traite_car('\b');
        }
    }
    place_curseur(1,0);
}
void place_curseur(uint32_t col, uint32_t lig)
{

    line = lig; //just retrieving the position of the cursor 
    column = col; 

    if(line>(1280/8-1))
    {
        traite_car('\n');
    }
    if(column>(720/8-1))
    {
        defilement();
    }
    curseur = (uint32_t *)(0x80000000 + 8*(line+column*1280)*4);
}

void traite_car(char c)
{
    uint16_t i =0, j=0;
    switch(c)
    {
        case '\b':
            while(i<8) //erase 
            {
                for(j = 0; j < 8 ; j++)
                {
                    pixel((line-1)*8 + j,column*8 + i, NULL);
                }
                i+=1;
            }
            if(line>0)//supposed to be line
                place_curseur(column, line-1);
            break; 
        case '\t':
            if(line==0)
            {
                place_curseur(column, line);
            }
            else
            {
                line+=(8 - line%8);
                place_curseur(column, line);
            } 
            flag_char=1; 
            break;
        case '\n':
            if(flag_cr) //if CR was pressed before, it shall return to the same line as before + 1
            {
                place_curseur(aux2+1, 0);
                flag_cr = 0;
            }
            else
            {
                place_curseur(column+1, 0);
            }
            break;
        case '\f':
            efface_ecran(); 
            break; 
        case '\r':
            flag_cr = 1; 
            while(line>0)
            {
                traite_car('\b');
            }
            aux1 = line; 
            aux2 = column;
            place_curseur(column, 0); 
            break;
        default:
            ecrit_car(line, column, c);
            //place_curseur(column, line+1);  
            if(!flag_char)
            {
                place_curseur(column,line+1);  
            }
            else
            {
                flag_char=0;
            }
    }
}
void defilement(void)
{
    uint32_t i = 0;

    /*place_curseur(1, 1280/8 -1);
    while(line>0)
    {
        traite_car('\b');
    }*/
    for(i = 1; i<(720/8 - 1); i++)
    {
        place_curseur(i, 0);
        memmove(curseur, curseur + 1280*8, 1280*8*sizeof(uint32_t));
    }
    place_curseur(89, 1280/8 - 1);
    while(line>0)
    {
        traite_car('\b');
    }
    place_curseur(89, 0);
}
void console_putbytes(const char *s, int len)
{
    int i=0; 
    for (i = 0; i < len; i++)
    {
        traite_car(s[i]); 
    }
}
