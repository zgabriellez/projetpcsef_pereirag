#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <temps.h>

#ifndef PROCESSUS_H
#define PROCESSUS_H

#define TAILLE 30 
#define PILLE 4096
#define NOMBRE 4


extern void ctx_sw(uint64_t *ctxt_de_sauvegarde, uint64_t *ctxt_a_restaurer);

enum etats {ELU = 1, ACTIVABLE = 2, ENDORMI = 3, MORT = 4};

typedef struct Processus
{
    int32_t  pid; 
    char *s; // MAX TAILLE = 30
    enum etats etat;
    uint32_t heureReveil; 
    uint64_t zones[17];     
    uint64_t pile[PILLE]; 
} process;

void idle(void);
void proc1(void); 
void proc2(void);
void proc3(void);

void init_proc(); 

void ordonnance(void);

int32_t cree_processus(void (*func)(void), char *s);
int32_t mon_pid(void);
char *mon_nom(void);

void dors(uint32_t nbr_secs); 
//void fin_processus(); 

#endif
